﻿
namespace Simulaciones_de_procesos
{
    partial class ucTanque
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.tmrLlenar = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciar = new System.Windows.Forms.Timer(this.components);
            this.timerActuadores = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelTanque = new System.Windows.Forms.Panel();
            this.panelAgua = new System.Windows.Forms.Panel();
            this.btnParo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbxVaciar = new System.Windows.Forms.PictureBox();
            this.pbxLlenar = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelTanque.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(17, 28);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(75, 23);
            this.btnLlenar.TabIndex = 2;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Location = new System.Drawing.Point(17, 57);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(75, 23);
            this.btnVaciar.TabIndex = 2;
            this.btnVaciar.Text = "Vaciar";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // tmrLlenar
            // 
            this.tmrLlenar.Interval = 50;
            this.tmrLlenar.Tick += new System.EventHandler(this.tmrLlenar_Tick);
            // 
            // tmrVaciar
            // 
            this.tmrVaciar.Interval = 50;
            this.tmrVaciar.Tick += new System.EventHandler(this.tmrVaciar_Tick);
            // 
            // timerActuadores
            // 
            this.timerActuadores.Enabled = true;
            this.timerActuadores.Tick += new System.EventHandler(this.timerActuadores_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.panelTanque);
            this.panel1.Location = new System.Drawing.Point(266, 180);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(24, 207);
            this.panel1.TabIndex = 6;
            // 
            // panelTanque
            // 
            this.panelTanque.BackColor = System.Drawing.Color.Black;
            this.panelTanque.Controls.Add(this.panelAgua);
            this.panelTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTanque.Location = new System.Drawing.Point(4, 4);
            this.panelTanque.Name = "panelTanque";
            this.panelTanque.Size = new System.Drawing.Size(16, 199);
            this.panelTanque.TabIndex = 0;
            // 
            // panelAgua
            // 
            this.panelAgua.BackColor = System.Drawing.Color.Blue;
            this.panelAgua.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAgua.Location = new System.Drawing.Point(0, 101);
            this.panelAgua.Name = "panelAgua";
            this.panelAgua.Size = new System.Drawing.Size(16, 98);
            this.panelAgua.TabIndex = 0;
            // 
            // btnParo
            // 
            this.btnParo.Location = new System.Drawing.Point(17, 86);
            this.btnParo.Name = "btnParo";
            this.btnParo.Size = new System.Drawing.Size(75, 23);
            this.btnParo.TabIndex = 2;
            this.btnParo.Text = "Paro";
            this.btnParo.UseVisualStyleBackColor = true;
            this.btnParo.Click += new System.EventHandler(this.btnParo_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.pbxVaciar);
            this.panel2.Controls.Add(this.pbxLlenar);
            this.panel2.Controls.Add(this.btnLlenar);
            this.panel2.Controls.Add(this.btnVaciar);
            this.panel2.Controls.Add(this.btnParo);
            this.panel2.Location = new System.Drawing.Point(88, 199);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(139, 140);
            this.panel2.TabIndex = 9;
            // 
            // pbxVaciar
            // 
            this.pbxVaciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxVaciar.Location = new System.Drawing.Point(101, 58);
            this.pbxVaciar.Name = "pbxVaciar";
            this.pbxVaciar.Size = new System.Drawing.Size(24, 22);
            this.pbxVaciar.TabIndex = 3;
            this.pbxVaciar.TabStop = false;
            // 
            // pbxLlenar
            // 
            this.pbxLlenar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxLlenar.Location = new System.Drawing.Point(101, 28);
            this.pbxLlenar.Name = "pbxLlenar";
            this.pbxLlenar.Size = new System.Drawing.Size(24, 22);
            this.pbxLlenar.TabIndex = 3;
            this.pbxLlenar.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Simulaciones_de_procesos.Properties.Resources.Recurso_1tank_7201;
            this.pictureBox1.Location = new System.Drawing.Point(233, 105);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(360, 296);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // ucTanque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel2);
            this.Name = "ucTanque";
            this.Size = new System.Drawing.Size(694, 498);
            this.panel1.ResumeLayout(false);
            this.panelTanque.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Timer tmrLlenar;
        private System.Windows.Forms.Timer tmrVaciar;
        private System.Windows.Forms.Timer timerActuadores;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelTanque;
        private System.Windows.Forms.Panel panelAgua;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnParo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbxVaciar;
        private System.Windows.Forms.PictureBox pbxLlenar;
    }
}
